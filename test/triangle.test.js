const Triangle = require('../src/triangle').Triangle;
const expect = require('chai').expect;

describe('Testing the Triangle Functions', function() {
    it('1. The base length of the Triangle', function(done) {
        let t1 = new Triangle(10, 10);
        expect(t1.getBaseLength()).to.equal(10);
        done();
    });
    
    it('2. The height of the Triangle', function(done) {
        let t2 = new Triangle(5, 8);
        expect(t2.getHeight()).to.equal(8);
        done();
    });
    
    it('3. The surface area of the Triangle', function(done) {
        let t3 = new Triangle(8, 8);
        expect(t3.getSurfaceArea()).to.equal(32);
        done();
    });

    it('4. Check if triangle is Isosceles', function(done) {
        let t4 = new Triangle(8, 8, 5, 5);
        expect(t4.checkifTriangleIsIsosceles()).to.equal(true);
        done();
    });
    
    it('5. Check if triangle is equilateral', function(done) {
        let t5 = new Triangle(8, 8, 5, 5);
        expect(t5.checkifTriangleIsEquilateral()).to.equal(false);
        done();
    });
});