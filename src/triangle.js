class Triangle {
    constructor(base, height, side1, side2) {
        this.base = base;
        this.height = height;
        this.side1 = side1,
        this.side2 = side2
    }

    getBaseLength () {
        return this.base;
    }
    
    getHeight () {
        return this.height;
    }

    getSurfaceArea() {
        return (this.base * this.height)/2;
    }

    checkifTriangleIsIsosceles() {
        let isIsosceles = false;
        if (this.side1 === this.side2) {
            isIsosceles = true;
        }
        return isIsosceles;
    }

    checkifTriangleIsEquilateral() {
        let isEquilateral = false;
        if (this.side1 === this.side2 && this.side1 === this.base) {
            isEquilateral = true;
        }
        return isEquilateral;
    }
}

module.exports = {
    Triangle:Triangle
}